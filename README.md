
# Star View and Star Cache
#### Star Wars API Viewer


Star View     | Star Cache
:------------:|:--------------:
![](https://assets.gitlab-static.net/uploads/-/system/project/avatar/10656353/sv-icon.png?width=64)  |  ![](https://assets.gitlab-static.net/uploads/-/system/project/avatar/10657375/sc-icon.png?width=64)
[![build status](https://gitlab.com/puzzleduck/star-view/badges/master/build.svg)](https://gitlab.com/puzzleduck/star-view/commits/master)  |  [![pipeline status](https://gitlab.com/puzzleduck/star-cache/badges/master/pipeline.svg)](https://gitlab.com/puzzleduck/star-cache/commits/master)
 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)  |   [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

### Intro

My submission for the SideKicker developer challenge is in two repositories, one for the frontend named [Star View](https://gitlab.com/puzzleduck/star-view) and the second for the backend called [Star Cache](https://gitlab.com/puzzleduck/star-cache.git).

The frontend is a React JS application [deployed to GitLab Pages](https://gitlab.com/puzzleduck/star-view/pipelines). The application is broken down into three main components with many small sub-components. The `src/App.js` module is responsible for maintaining the list of people and invoking the other components. `PersonListing.js` takes care of most of the list/table structure. `PersonEntry.js` then displays the individual line entries and contains a hidden sub-component for displaying the details pane. If time allowed my next move on the frontend would be to extract out the details panel into a seperate component.

The backend is a PHP/Silex application backed by a postgres database and deployed to Heroku. The main components of the application are the `src/controllers.php` module which handles the routing for the application, `src/Models/Person.php` which holds the data model for our application and the files in `src/SwapiConnector`. The SwapiConnector folder holds all the data sources for the application which are split between the `SwapiCache.php` module which caches results in a Postgres database and `SwapiApi.php` which handles requests to the real SWAPI. The `SwapiConnector.php` module mediates the use of the raw api and the caching mechanism and it was emerging as a Chain of Responsibility pattern where if the caching failed it would delegate the request to the api calls, however I was still in the process of this refactor when I needed to prepare my submission.

Both parts of the project are publicly hosted and accessible. For example you can request the  list of characters by opening the following link in your browser [https://star-cache.herokuapp.com/web/index.php/people/](https://star-cache.herokuapp.com/web/index.php/people/) or better yet open it in postman! A much more user friendly version of the same information can be seen by viewing the deployment of the frontend at [https://puzzleduck.gitlab.io/star-view/](https://puzzleduck.gitlab.io/star-view/)

As can be seen in the graphs below, the backend and frontend were built using a branching GitFlow like workflow and we can also see a pattern of Test Driven Development especially in the backend where there is a strong 'test'/'feature' alternation.

Backend Git Log Graph            | Frontend Git Log Graph
:-------------------------------:|:----------------------------------------:
![](https://puzzleduck.gitlab.io/star-view/backend-log.png)        | ![](https://puzzleduck.gitlab.io/star-view/frontend-log.png)

The Star Cache backend has reasonably comprehensive unit tests for the data model and components using PhpUnit. The Star View frontend uses Jest to verify the rendering of all components. If time allowed I would work on mocking a data source for the backend tests and then implementing end to end testing of the system as a whole. I also used the jsonlint.com service to verify the integrity of the data produced by Star Cache.

Both the frontend and the backend have been documented in the Star View project however it would be nice to mirror the documentation in the backend project. Both projects also contain supporting documents such as licence information, code of conduct and contributor documentation. The Star View project also has a [wiki](https://gitlab.com/puzzleduck/star-view/wikis/home) to help users get started and connect with one another.

The frontend Star View is automatically tested and built by a GitLab CI pipeline and is automatically deployed when branches are merged into master. The backend is manually deployed by executing a `git push heroku master` command and could easily be automated if desired.

Some of the components in the back end are seeming to converge on possible interfaces such as in the api and caching modules. If i had further time I would begin extracting those interfaces, however some consideration may be required as the two data sources are serving different purposes. The process of extracting the two modules is ongoing the current focus of development.



### Getting Started

To run the application we will need to clone both the frontend and the backend:
```
git clone https://gitlab.com/puzzleduck/star-cache
git clone https://gitlab.com/puzzleduck/star-view
```

First we'll go into the backend, install the dependencies and run the tests:
```
cd star-cache
composer install
./vendor/bin/phpunit --stderr
```

Now we can run Star Cache in the background:
```
php -S localhost:8088 -t web &
```

Star Cache should now be running and listening on port `8088`. We can verify this by requesting the data for the first person:
```
curl http://localhost:8088/person/1
```

Now we can get cracking on the frontend, first install the dependencies and run the tests:
```
cd ../star-view
npm install
CI=true npm test
```

We can also create a production build locally, but this is done automatically for us in the deployment pipeline but can be a useful test during local development.
```
npm run build
```

Now we're ready to run the local server:
```
npm start &
```

Finally we can open a browser to [http://localhost:3000/](http://localhost:3000/) and view the Star View application.



### Issues and assumptions

The backend seemed like it would not accomplishes much and I created an internal data model to give it some work to do rather than just mirroring responses. However in the end the backend provides simplification of the data down to minimal levels speeding up applications. It also caches responses and minimises the amount of traffic hitting the original api. It also allows us to have direct control over access control, for example setting access control headers.

The PHP SWAPI libraries recommended by the api both had issues, one no longer exists and the examples in the other library failed to run... maybe that was just me, but in any case I wrote my own requests and parsers for the api.

GitHub Pages does not seem to handle redirects in the React application I have setup, however I was able to avoid this issue by creating a single page application with dropdown information for the details view.


### Plan

While I would not normally keep the following checklist in the repository I have included my interactive plan here for your consideration. The plan has been updated over the course of the project and can be seen to develop over time in the git revisions. It does not represent fore knowledge of how everything would pan out (if only).

- [x] Review: challenge
- [x] Outline plan
- [x] Git repo
- [x] Review: Sidekicker code and site, minimal public code (react and Kohana - ORM forks)
- [x] Docs: licence, contrib, coc...
- [x] Code: setup frontend
- [x] Code: frontend tests - jest
- [x] Code: frontend deployment: gitlab pages
- [x] Code: frontend pipeline
- [x] Code: setup backend
- [x] Code: backend tests
- [x] Code: backend deployment: heroku
- [x] Code: backend pipeline
- [x] Review: plan
- [x] Code: develop backend
- [x] Backend: data model
- [x] Backend: simple mock data
- [x] Backend: routing
- [x] Backend: return list of people
- [x] Backend: return person detail
- [x] Code: develop frontend
- [x] Frontend: display static info (title)
- [x] Frontend: mock data
- [x] Frontend: create list and associated components
- [x] Frontend: display list of people
- [x] Frontend: routing
- [x] Frontend: detail components
- [x] Frontend: display person detail
- [x] Backend: retrieve live data from swapi
- [x] Backend: return live data
- [x] Frontend: retrieve live data from star-cache
- [x] Frontend: fix deployment - spa
- [x] Frontend: details view
- [x] Bonus: alternative ui components (eg dropdown)
- [x] Backend: planet and film lookups
- [x] Backend: backend storage
- [x] Docs: readme
- [x] Frontend: expansion indicator (icon/mouseover)
- [x] Frontend: layout and review frontend
- [x] Bonus: images
- [x] Backend: cors for localhost
- [x] Review: SOLID
- [x] Review: js/react standards
- [x] Review: php standards
- [x] Review: readability
- [x] Docs: wiki
- [x] Backend: review backend
- [ ] Submit


### Challenge text

Star Wars Coding Challenge

- [x] Build a Star Wars API (SWAPI) Viewer
- [x] The challenge is to build a simple viewer app for the Star Wars API ( https://swapi.co/ ).

- [x] This SWAPI Viewer needs to do the following:
- [x] Connect to the Star Wars API
- [x] Request all the People from the SWAPI
- [x] Display the People in a list

- [x] Selecting an item in the list brings up details about them, such as:
- [x] Height
- [x] Hair Color
- [x] Birth Year
- [x] Name of their Homeworld
- [x] Title of films they appear in

- [x] Requirements
- [x] (JS frontend) <---> (PHP backend) <---> (SWAPI)
- [x] There should be two components to your submission
- [x] a frontend web app built in JavaScript
- [x]  a backend app built in PHP.
- [x] The frontend should connect to the backend to request information.
- [x] The backend should communicate with the SWAPI service to retrieve the relevant information requested.
- [x] For the backend please use a decent framework. Some examples are:
- [x] Silex https://silex.symfony.com/ (PHP)
- [x] For the frontend please use React ( https://reactjs.org/ ).
- [x] Please include a README with installation and usage instructions.
- [x] Please include some attempt at testing your code.

- [x] Assessment Criteria
- [x] Submission meets requirements.
- [x] Submission demonstrates some knowledge of application design:
- [x] Separation of concerns
- [x] Simplicity
- [x] Includes tests.
- [x] UI is easy to use and displays information clearly.
- [x] Code demonstrates consistency and adherence to common standards.

- [x] Some pointers
- [x] The backend will require some kind of HTTP client library to connect to the SWAPI.
- [x] The backend will need to parse the JSON responses from SWAPI for them to be useful.
- [x] SWAPI is well documented at https://swapi.co/documentation
- [x] There are PHP helper libraries for SWAPI that you could consider using. See SWAPI docs.

### Sidekicker

- Founded in Melbourne... Sydney
- anyone requiring a service should be able to connect with someone willing to provide it
- Loud and Clear - Avanade
- SEEK
- 7 cities across Australia and New Zealand
- Social Media

