import React, { Component } from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom'
import { BrowserRouter } from 'react-router-dom'
import PersonListing from './PersonListing'
import PersonEntry from './PersonEntry'

// The App component is resposible for handling routing, requesting the initial
// data payload and displaying other components within the application
class App extends Component {
  state = {
    people: []
  };

  // When the application starts we request data from the backend
  componentDidMount() {
    let url;
    if(process.env.PUBLIC_URL === ""){
      url = "http://localhost:8088/people";
    } else {
      url = 'https://star-cache.herokuapp.com/web/index.php/people/';
    }
    fetch(url) //
      .then(peopleData => {
        return peopleData.json();
      })
      .then(peopleJson => {
        this.setState({
          people: peopleJson
        });
      });
  }

  // In the main render method we display simple header information, setup
  // the router and create the PersonListing component
  render() {
    const { people } = this.state;
    const id_path = process.env.PUBLIC_URL + "/person/:id";
    return (
      <div className="App">
        <header className="App-header">
          <h1>Star View</h1>
          <h3>SWAPI Client and Mirror</h3>
        </header>
        <main className="App-body">
          <BrowserRouter basename={'/star-view'}>
            <Switch>
            <Route path={id_path} component={PersonEntry} />
            <Route path='/person/:id' component={PersonEntry} />
            </Switch>
          </BrowserRouter>
          <PersonListing people={people} />
        </main>
      </div>
    );
  }
}

export default App;
