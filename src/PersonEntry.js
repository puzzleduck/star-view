import React, {Component} from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';

// Display the short listing for a person
const PersonRow = props => {
  const { id, name, birth, clickHandler } = props;
  return(
    <Row key={id} style={{ cursor: 'pointer' }} onClick={clickHandler}>
      <IdEntry id={id} />
      <NameEntry name={name} />
      <BirthEntry birth={birth} />
    </Row>
  );
};

// Simple Id component
const IdEntry = props => {
  const { id } = props;
  return(
    <Col md={2}>
      <p>{id}</p>
    </Col>
  );
};

// Simple Name component
const NameEntry = props => {
  const { name } = props;
  return(
    <Col md={7}>
      <p>{name}</p>
    </Col>
  );
};

// Simple Birth component
const BirthEntry = props => {
  const { birth } = props;
  return(
    <Col md={3}>
      <p>{birth}</p>
    </Col>
  );
};

// Simple Height component
const HeightDetail = props => {
  const { height } = props;
  return(
    <Row>
      <Col md={6} className="col-right">Height:</Col>
      <Col md={6} className="col-left">{height}</Col>
    </Row>
  );
};

// Simple Hair component
const HairDetail = props => {
  const { hair } = props;
  return(
    <Row>
      <Col md={6} className="col-right">Hair Color:</Col>
      <Col md={6} className="col-left">{hair}</Col>
    </Row>
  );
};

// Simple Birth component
const BirthDetail = props => {
  const { birth } = props;
  return(
    <Row>
      <Col md={6} className="col-right">Birth Year:</Col>
      <Col md={6} className="col-left">{birth}</Col>
    </Row>
  );
};

// Simple Homeworld component
const HomeDetail = props => {
  const { home } = props;
  return(
    <Row>
      <Col md={6} className="col-right">Homeworld:</Col>
      <Col md={6} className="col-left">{home}</Col>
    </Row>
  );
};

// Film Listing component
const FilmDetail = props => {
  const { films } = props;
  return(
    <Row className="films">
      <Col>
        <br />
        <h3 className="yellow">Film Appearances:</h3>
        {films.map(function(film){
          return <p>{film}</p>
        })}
      </Col>
    </Row>
  );
};

// Component (initially hidden) for displaying the details of a person
class PersonEntry extends Component {
  // Initialize known values and placeholders
  constructor(props) {
    super(props);
    let { id, name, birth_year} = props.person;
    this.state = {
      isDetailVisible: false,
      'id': id,
      'name': name,
      'height': '-- loading --',
      'hair_color': '-- loading --',
      'birth_year': birth_year,
      'homeworld': '-- loading --',
      'films': ["-- loading --"]
    };
    this.toggleDetails = this.toggleDetails.bind(this);
  }

  // Function for hiding and showing the details pane
  toggleDetails() {
    let url;
    if(process.env.PUBLIC_URL === ""){
      url = "http://localhost:8088/person/";
    } else {
      url = "https://star-cache.herokuapp.com/web/index.php/person/";
    }
    if(this.state.homeworld === "-- loading --") {
      fetch(url + this.state.id)
        .then(personData => {
          return personData.json();
        })
        .then(personJson => {
          this.setState({
            height: personJson.height,
            hair_color: personJson.hair_color,
            homeworld: personJson.homeworld,
            films: personJson.films
          });
        });
    }
    this.setState(state => ({
      isDetailVisible: !state.isDetailVisible
    }));
  }

  // In render we display our current state with placeholders which are updated
  // as soon as the database or api respond
  render() {
    let { id, name, birth_year, isDetailVisible, height, hair_color, homeworld, films } = this.state;
    return(
      <div className="outer-row">
        <PersonRow id={id}
                   name={name}
                   birth={birth_year}
                   clickHandler={this.toggleDetails} />
        <div hidden={!isDetailVisible}
             style={{ cursor: 'pointer' }}
             onClick={this.toggleDetails}>
          <Row key={name}>
            <Col md={11}>
              <h3 className="yellow">Details</h3>
              <Grid>
                <HeightDetail height={height} />
                <HairDetail hair={hair_color} />
                <BirthDetail birth={birth_year} />
                <HomeDetail home={homeworld} />
                <FilmDetail films={films} />
              </Grid>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default PersonEntry;