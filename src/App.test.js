import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import PersonListing from './PersonListing'
import PersonEntry from './PersonEntry'
import { MemoryRouter } from "react-router-dom";

it('detials component renders', () => {
  const person = [{'name': 'Luke Skywalker', 'height': '172', 'hair_color': 'blond', 'birth_year': '19BBY', 'homeworld': 'homeworld1', 'films': ["film1", "film2"]}];
  const div = document.createElement('div');
  const props = {
    person: {
      id: '1'
    }
  }
  ReactDOM.render(<PersonEntry {...props} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('listing component renders', () => {
  const people = [{'id': '1', 'name': 'Luke Skywalker', 'birth_year': '19BBY'}];
  const div = document.createElement('div');
  ReactDOM.render(<PersonListing people={people} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('tautology', () => {
  expect(true).toEqual(true);
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
