import React, {Component} from 'react';
import PersonEntry from './PersonEntry';
import { Grid, Row, Col } from 'react-flexbox-grid';

// Display a simple header row for the listing
const Heading = () => {
  return(
    <div>
      <Row>
        <Col md={2}>
          <p className="yellow">Id</p>
        </Col >
        <Col md={7}>
          <p className="yellow">Name</p>
        </Col>
        <Col md={3}>
          <p className="yellow">Birth Year</p>
        </Col>
      </Row>
    </div>
  );
};

// Map the people array over the PersonEntry component
const People = props => {
  const tableRows = props.people.map((person, index) => {
    return(
        <PersonEntry person={person} key={index} />
    );
  });
  return(
    <div>
      {tableRows}
    </div>
  );
};


class PersonListing extends Component {
  render() {
    const { people } = this.props;
    return(
      <Grid fluid className="outer-table">
        <Heading />
        <People people={people} />
      </Grid>
    );
  }

}
export default PersonListing;

